  GNU nano 7.2                                                                          erstelleVorlage.sh                                                                                    
#!/bin/bash

# Erstelle das Verzeichnis "_templates"
mkdir _templates

# Erstelle mindestens 3 Dateien im "_templates"-Verzeichnis
touch _templates/datei-1.txt
touch _templates/datei-2.pdf
touch _templates/datei-3.doc

# Erstelle das Verzeichnis "_schulklassen"
mkdir _schulklassen

# Erstelle mindestens 2 Schulklassen-Dateien im "_schulklassen"-Verzeichnis
touch _schulklassen/M122-PE23d.txt
touch _schulklassen/M122-PE23e.txt
touch _schulklassen/M122-PE23f.txt

# Füge mindestens 12 Schüler-Namen zu jeder Schulklassen-Datei hinzu
echo "AninaRuckstuhl" > _schulklassen/M122-PE23d.txt
echo "AnnaMaligez" >> _schulklassen/M122-PE23d.txt
echo "FabianRuckstuhl" >> _schulklassen/M122-PE23d.txt
echo "PascalRuckstuhl" >> _schulklassen/M122-PE23d.txt

echo "MarcoRuckstuhl" > _schulklassen/M122-PE23e.txt
echo "SaschaBankay" >> _schulklassen/M122-PE23e.txt
echo "AntonSauter" >> _schulklassen/M122-PE23e.txt
echo "StefanRuckstuhl" >> _schulklassen/M122-PE23e.txt

echo "CemKernen" > _schulklassen/M122-PE23f.txt
echo "ArcherPharao" >> _schulklassen/M122-PE23f.txt
echo "VincentMueller" >> _schulklassen/M122-PE23f.txt
echo "JoelleSpielmann" >> _schulklassen/M122-PE23f.txt

# Gib eine Erfolgsmeldung aus
echo "Skript erfolgreich ausgeführt!"
